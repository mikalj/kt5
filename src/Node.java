
import java.util.*;

public class Node {

   private String name;
   private Node firstChild;
   private Node nextSibling;

   Node (String n, Node d, Node r) {
      name = n;
      nextSibling = d;
      firstChild = r;

   }
   
   public static Node parsePostfix (String s) {
      StringTokenizer juta = new StringTokenizer(s, ",()", true);
      String jutaPost = "";
      int betweenBraces = 0;
      boolean isZeroBraces = false;
      while (juta.hasMoreTokens()){
         if (betweenBraces == 0){
            isZeroBraces=true;
         }
         String token = juta.nextToken();
         if (token.equals("(")){
            betweenBraces++;
         }
         else if (token.equals(")")){
            betweenBraces--;
         }
         else if (token.equals(",")){
            continue; //tegelt mdea mis see veel tegema peaks
         }
         if (betweenBraces != 0 && !isZeroBraces) {
            jutaPost += token;
         }
         isZeroBraces=false;
         if (betweenBraces == 0){
            return parsePostfix(jutaPost);
         }
      }
      /*((((E)D)C)B)A*/
      String rootName = juta.nextToken();
      return new Node(rootName, null, null);
   }

   public String leftParentheticRepresentation() {
      String representation = this.name;
      if (this.firstChild != null){
         firstChild.leftParentheticRepresentation();
      }
      else if (nextSibling != null){
         nextSibling.leftParentheticRepresentation();
      }
      return this.name;
   }

   public static void main (String[] param) {
      String s = "(B1,C)A";
      Node t = Node.parsePostfix (s);
      String v = t.leftParentheticRepresentation();
      System.out.println (s + " ==> " + v); // (B1,C)A ==> A(B1,C)
   }
}
